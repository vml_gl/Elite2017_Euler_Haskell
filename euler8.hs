slices = map (take 13) (tails n)
ans = maximum [product $ map digitToInt x | <- slices, length == 13]
