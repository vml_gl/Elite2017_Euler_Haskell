main = putStrLn (show ans)
factors n = [x | x <- [1..n], mod x n == 0]
isprime n = factors n == [1,n]
primes = [x | x <- [1..10], isprime x]
ans = primes !! 2 2 
