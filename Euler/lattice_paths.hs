main = putStrLn (show ans)
ans = (binomial 40 20)
binomial n k = div (product [(n - k + 1) .. n]) (product [1..k])
