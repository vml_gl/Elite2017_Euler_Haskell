ans = max [a*b | a <- [100..999], b <- [100..999], is_palindrome (a*b)]
reverse_int = reverse 0 where
    reverse acc 0 = acc
    reverse acc n = reverse (acc * 10 + (mod n 10)) (div n 10)
is_palindrome n = n == (reverse_int n)
