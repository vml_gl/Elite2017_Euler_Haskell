ans = largestprime (600851475143)
largestprime n = 
    let 
        p = smallestprime n
    in
        if p == n then p 
        else largestprime (div n p)
smallestprime n = head [k | k <- [2..n], mod n k == 0]
