main = putStrLn (show ans)
ans = multiples 48
divisors n = length [x | x <- [1..n], mod n x == 0] 
multiples x = length[(i,j)| i <- [1..x], j <- [1..x], i < j, i * j == x, divisors i == divisors j]

