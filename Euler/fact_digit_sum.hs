main = putStrLn(show ans)
ans = digit_sum(fact 5)
fact 0 = 1
fact n = product [1..n]
digit_sum 0 = 0
digit_sum x = mod x 10 + digit_sum(div x 10)
