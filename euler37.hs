n = putStrLn (ans)
ans = sum $ take 11 $ filter test $ filter (>7) primes
factors n = [x | x <- [1..n], mod n x == 0]
isprime n = factors n == [1,n]
primes = [x | x <- [1..], isprime x]
test' n d f
        | d > n = True
        | otherwise = isprime (f n d) && test' n (10*d) f
test n = test' n 10 (mod) && test' n 10 (div)

